<div class="p-x">
    <div class="col-mx">
        <a href="<?PHP echo $btn_add?>" class="btn t1 green">TAMBAH SISWA</a>
    </div>

    <?PHP echo flashdata('sukses') ?>

    <div class="col-mx">
        <table class="f-table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?PHP 
                    $no = 1;
                    foreach($data_siswa as $ds): 
                ?>
                <tr>
                    <td><?PHP echo $no++ ?></td>
                    <td><?PHP echo $ds->nama ?></td>
                    <td><?PHP echo $ds->nama_kelas ?></td>
                    <td><?PHP echo $ds->keterangan ?></td>
                    <td>
                        <a href="<?PHP echo $btn_edit . $ds->id ?>" class="btn t1 blue">EDIT</a>
                        <a href="<?PHP echo $btn_del  . $ds->id ?>" class="btn t1 red delete">DELETE</a>
                    </td>
                </tr>
                <?PHP 
                    endforeach; 
                ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).on("click", ".delete", function (x) {
        if(confirm("Apakah Anda yakin ingin menghapus data tersebut?"))
        {
            window.location.href = $(this).attr("href");
        }
        x.preventDefault();
    });
</script>