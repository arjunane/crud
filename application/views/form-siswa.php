<div class="col-mx">
    <a href="<?PHP echo $btn_back ?>" class="btn t1 blue">BACK</a>
</div>
<div class="col-6">
    <?PHP echo $form; ?>
        <div class="col-mx">
            <?PHP echo form_error('--nama'); ?>
            <div class="f-input">
                <span>Nama Siswa</span>
                <input type="text" name="--nama" placeholder="Masukkan Nama Siswa..." value="<?PHP echo $_nama ?>">
            </div>
        </div>
        <div class="col-mx">
            <?PHP echo form_error('--kelas'); ?>
            <div class="col-mx">
                <span>Kelas Siswa</span>
            </div>
            <?PHP foreach($data_kelas as $kl): ?>
                <div class="f-check blue">
                    <input type="radio" name="--kelas" value="<?PHP echo $kl->id ?>" <?PHP if($kl->id == $_kelas) echo "checked" ?> >
                    <span><?PHP echo $kl->nama_kelas; ?></span>
                    <label></label>
                </div>
            <?PHP endforeach; ?>
        </div>
        <div class="col-mx">
            <?PHP echo form_error('--keterangan'); ?>
            <div class="f-input">
                <span>Keterangan</span>
                <textarea name="--keterangan" cols="30" rows="10" placeholder="Masukkan Keterangan ..."><?PHP echo $_keterangan ?></textarea>
            </div>
        </div>
        <div class="col-mx center">
            <button class="btn t1 green">SIMPAN</button>
        </div>
    </form>
</div>