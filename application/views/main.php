<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="<?PHP echo $css ?>">
	<script src="<?PHP echo $jquery ?>" type="text/javascript"></script>
	<title><?PHP echo $title ?></title>
</head>
<body>
	<?PHP echo $header; ?>
	<div class="p-xxx">
		<?PHP echo $section; ?>
	</div>
	<?PHP echo $footer; ?>
	<script src="<?PHP echo $main ?>" type="text/javascript"></script>
</body>
</html>