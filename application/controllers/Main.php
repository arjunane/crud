<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// nama class controller : Huruf pertama harus besar

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		// load model Dbs
		$this->load->model("dbs");
	}
	
	/*
		$title = untuk judul halaman
		$view  = untuk view utama

		protected digunakan untuk function yang hanya di akses secara private dengan catatan nama function nya harus mempunyai underscore ( _ ) pada awal penamaan function
	*/
	protected function _mainMenu($title, $view)
	{
		$data['css']		= css('arjunane.min.css');
		$data['jquery']		= js('jquery.js');
		$data['main']		= js('main.js');
		
		$data['title'] 		= $title;
		/*
			view("file", data yang akan di include, TRUE = string)
		*/
		$data['header']		= view('aset/header','', TRUE);
		$data['section']	= $view;
		$data['footer']		= view('aset/footer', '', TRUE);

		return view('main', $data);
	}

	function index()
	{
		/*
			ADA DUA CARA PENGGUNAAN
			1.	$data .....;
				$view 	= view('home', $data, TRUE);
				$title 	= "Menu Utama";
				$this->_mainMenu($title, $view);
			2.  $data .....;
				$this->_mainMenu( "Menu Utama", view("home", $data, TRUE) );

			pastikan parameters terakhir/ketiga pada view harus TRUE (di $this->_mainMenu)
		*/
		$data['btn_add']  	= site_url('main/siswa/tambah/');
		$data['btn_edit'] 	= site_url('main/siswa/update/');
		$data['btn_del']  	= site_url('main/siswa/delete/');
		$data['data_siswa'] = $this->dbs->all_siswa();
		$view = view('home', $data, TRUE);
		$this->_mainMenu('Menu Utama', $view);
	}
	
	function siswa($kategori = NULl, $id_siswa = NULl)
	{
		/*
			($kategori == "tambah" && $id_siswa == NULL) 
				-> jika $kategori sama dengan TAMBAH dan $id_siswa sama dengan NULl maka form tambah siswa
			ATAU
			($kategori == NULL && $id_siswa != NULL) 
				-> jika $kategori sama dengan UPDATE dan $id_siswa tidak sama dengan NULl maka form edit/update siswa
		*/
		if( ($kategori == "tambah" && $id_siswa == NULL) || ($kategori == "update" && $id_siswa != NULL) )
		{
			// jika ada validasi pada input, pastikan load library "form_validation" telebih dahulu
			$this->load->library('form_validation');


			$data['form']   	= form_open("main/siswa/" . $kategori . "/" . $id_siswa);
			$data['btn_back']	= site_url('main/');

			$data['data_kelas'] = $this->dbs->result('kelas');	

			$siswa;
			// jika parameters $kategori == "update"
			if($kategori == "update")
			{
				// validasi $id_siswa, apakah ada data siswa sesuai dengan parameters $id_siswa ?!
				$siswa = $this->dbs->row_array("id", $id_siswa, "siswa");
				if(!$siswa) die("-=[]=-"); // jika data siswa berdasarkan $id_siswa tidak ada, maka langsung die();
			}

			set_rules("--nama",  	  "Nama Siswa",  	  "required|max_length[50]");
			set_rules("--kelas", 	  "Kelas Siswa", 	  "required|max_length[2]");
			set_rules("--keterangan", "Keterangan Siswa", "required|max_length[500]");

			// style html ketika terjadi error saat validasi
			set_error_delimiters('<div class="form-error"><span>', "</span></div>");

			// digunakan untuk mengganti info error dengan bahasa kita
			// %s sesuai dengan parameters kedua pada set_rules();
			set_message("required", "%s harap di isi.");
			set_message("max_length", "Maksimal %s ialah %s karakter.");

			/*
				posts("name_field pada html"); / $this->input->post(); / $_POST[""]
				
				set_value();
				menge-check apakah ada perbedaan text antara posts() dengan data pada database
				jika berbeda antara post dengan data di database, maka yang diambil berdasarkan posts() / $_POST[]
			*/

			/*
				jika kategori sama dengan tambah
				-> tambah = tanda tanya atau ? maka mendapatkan data yang ada di posts() 	|| KATEGORI TAMBAH
				-> update = titik dua atau : jika tidak maka check_value()					|| KATEGORI UPDATE / EDIT
			*/
			$data["_nama"] 		 = $kategori == "tambah" ? posts("--nama") 		 : check_value(posts("--nama"), 		$siswa['nama']);
			$data["_kelas"] 	 = $kategori == "tambah" ? posts("--kelas") 	 : check_value(posts("--kelas"), 		$siswa['id_kelas']);
			$data["_keterangan"] = $kategori == "tambah" ? posts("--keterangan") : check_value(posts("--keterangan"), $siswa['keterangan']);


			// jika validasi sesuai dengan parameter tiga pada set_rules();
			if(valid_run())
			{
				$save['nama'] 		= posts("--nama");
				$save['id_kelas'] 	= posts("--kelas");
				$save['keterangan'] = posts("--keterangan");

				$save_data = $kategori == "tambah" ? 
								$this->dbs->insert($save, 'siswa') : 
								$this->dbs->update('id', $siswa['id'], $save, 'siswa');

				set_flashdata('sukses', info_success('Data berhasil di simpan'));
				redirect('main/');
			}

			// pastikan saat $this->_mainMenu() dilakukan pada akhir statement
			$this->_mainMenu("FORM SISWA", view("form-siswa", $data, TRUE) );
		}
		else if($kategori == "delete" && $id_siswa != NULL)
		{
			// validasi $id_siswa, apakah ada data siswa sesuai dengan parameters $id_siswa ?!
			$siswa = $this->dbs->row_array("id", $id_siswa, "siswa");
			if(!$siswa) die("-=[]=-"); // jika data siswa berdasarkan $id_siswa tidak ada, maka langsung die();

			$this->dbs->delete('id', $siswa['id'], 'siswa');

			set_flashdata('sukses', info_success('Data berhasil di hapus'));
			redirect("main/");
		}
	}
}
