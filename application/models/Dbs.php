<?php
defined('BASEPATH') or exit("rika mau ngapa kang? :/");
class Dbs extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
        function row_array  = untuk menampilkan SATU data saja
        function result     = untuk menampilkan semua data berdasarkan nama tabel tanpa where
        function result_key = untuk menampilkan semua data berdasarkan where
        function insert     = untuk menyimpan data
        function update     = untuk meng-update data
        function delete     = untuk menghapus data

        $field  = nama field pada table (contoh: id / nama_siswa, id_kelas, dll)
        $value  = value berdasarkan $_POST / $_GET
        $table  = nama table
        $data   = data yang akan di update / insert
        $sort   = untuk penyesuaian pengurutan data berdasarkan field tertentu
    */
    # MAIN STRUKTUR DATABASE
    function row_array($field, $value, $table)
    {
        return $this->db->where($field, $value, TRUE)->get($table)->row_array();
    }

    function result($table, $sort = "id ASC")
    {
        return $this->db->order_by($sort)->get($table)->result();
    }
    
    function result_key($field, $value, $table, $sort = "id ASC")
    {
        return $this->db->where($field, $value, TRUE)->order_by($sort)->get($table)->result();
    }
    
    function insert($data, $table)
    {
        return $this->db->insert($table, $data);
    }

    function update($field, $value, $data, $table)
    {
        return $this->db->where($field, $value, TRUE)->update($table, $data);
    }

    function delete($field, $value, $table)
    {
        return $this->db->where($field, $value, TRUE)->delete($table);
    }
    # END MAIN STRUKTUR DATABASE

    // penggunaan join
    function all_siswa()
    {
        return $this->db->select("siswa.*,kelas.nama_kelas, kelas.id as id_kelas ")->join('kelas', 'siswa.id_kelas = kelas.id')->get('siswa')->result();
    }
}